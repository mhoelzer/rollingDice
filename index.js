const count = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
const results = [];

function rollOne() {
    // need the + 1 because this starts at 0, so now it can go from 1-6 instead of 0-5
    return Math.floor(((Math.random() * 6) + 1));
}
function rollTwo() {
    return rollOne() + rollOne()
    // have it be plus 2 because it needs to start at 2 since youre adding 2 die together
    // actually, dont do this because this is more like rolling a 12-sided die instead of 2 sixsided die, so change to use rollone
    // return Math.floor(((Math.random() * 11) + 2));
}

function diceValues() {
    for (var i = 1; i <= 1000; i++) {
        count[rollTwo()]++;
    }
}

function numberedResults() {
    for(let i = 2; i <=12; i++) {
        let div = document.createElement("div");
        // the i is what number you are looking at/the number where let i = 2; the couunt[i] is showing how much is in that current index
        let text = document.createTextNode(i + ": " + count[i]);
        div.appendChild(text);
        let placeHere = document.getElementById("results");
        placeHere.appendChild(div);
    }
}

function barredResults() {
    for(let i = 2; i <=12; i++) {
        let div = document.createElement("div");
        // let text = document.createTextNode(i + ": " + count[i]);
        // div.appendChild(text);
        div.className = "rectangle";
        div.style.height = "30px";
        div.style.width = count[i] + "px";
        div.style.backgroundColor = "orange";
        div.style.margin = "2px";
        let placeHere = document.getElementById("results");
        placeHere.appendChild(div);
    }
}

diceValues();
numberedResults();
barredResults();